﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Resturent
{
    public partial class frmitem : Form
    {
        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;
      

        public frmitem()
        {
            InitializeComponent();
         
            con = Connect.connect();
            loadcatogory();
        }

        public void loadcatogory()
        {
            try
            {

                openCon();


                String s1 = "select * from catogory";

                mc = new MySqlCommand(s1, con);
                mdr = mc.ExecuteReader();
                cmbcatogory.Items.Clear();

                while (mdr.Read())
                {


                    cmbcatogory.Items.Add(mdr.GetString(1));


                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }


        }

        private void btnsave_Click(object sender, EventArgs e)
        {


            if (txtitemcode.Text == "")
            {
                MessageBox.Show("Please enter product Code ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtitemcode.Focus();
                return;
            }
            if (txtitemname.Text == "")
            {
                MessageBox.Show("Please enter Product Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtitemname.Focus();
                return;
            }
            if (txtprice.Text == "")
            {
                MessageBox.Show("Please enter price", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtprice.Focus();
                return;
            }
            try
            {
                openCon();

                string s = "select itemid from item where itemid='" + txtitemcode.Text + "'";

                mc = new MySqlCommand(s, con);
                mdr = mc.ExecuteReader();
                //
                if (mdr.Read())
                {
                    MessageBox.Show("Product Name Already Exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtitemcode.Text = "";
                    txtitemcode.Focus();


                    if ((mdr != null))
                    {
                        mdr.Close();
                    }
                    return;
                }
                closeCon();
               
                String q = "insert into item (itemid ,itemname,price,catogory) values ('" + txtitemcode.Text + "','" + txtitemname.Text + "','" + Double.Parse(txtprice.Text) + "','"+ cmbcatogory.SelectedItem.ToString() +"') ";
                    ExecuteQuery(q);

                    this.txtitemcode.Text = "";
                    this.txtitemname.Text = "";
                    this.txtprice.Text = "";
                
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
                

                frmitemmanager.K = 1;
                //frmitemmanager f = new frmitemmanager();
               // Form obj = Application.OpenForms["ITEM"];
             //   obj.insertTable();
                //datagridview1.update();
               // datagridview1.refresh();
                //f.
            }


        }
        //function to open connection
        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        //function to execute the insert update and delete query
        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("New Item Save");
                   
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }

        private void lblnew_Click(object sender, EventArgs e)
        {
            String s = Interaction.InputBox("Enter Your Catogary");
            String quary = "Insert into catogory (catogoryname) values ('" + s + "')";
            ExecuteQuery(quary);
            loadcatogory();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       




    }
}
