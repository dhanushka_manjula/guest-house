﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Resturent
{
    public partial class frmitemupdate : Form
    {
        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;
        String s;
        String s2;
        String id;
      

        public frmitemupdate()
        {
            InitializeComponent();
           // this.cmbcatogory.SelectedIndex = 0;
            con = Connect.connect();
        
           s = frmitemmanager.itemid();
           loadcatogory();
            itemload0();
        }

        public void loadcatogory(){
            try
            {

                openCon();


                String s1 = "select * from catogory";

                mc = new MySqlCommand(s1, con);
                mdr = mc.ExecuteReader();
                cmbcatogory.Items.Clear();

                while (mdr.Read())
                {
                   
                    
                    cmbcatogory.Items.Add ( mdr.GetString(1));
                   
                   
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }


        }

        void itemload0()
        {
            // = s;

            try{

            openCon();


            String s1 = "select * from item where itempk ='"+s+"'";

            mc = new MySqlCommand(s1, con);
            mdr = mc.ExecuteReader();
        
            if (mdr.Read())
            {
                txtitemcode.Text = mdr.GetString(1);
                id = mdr.GetString(1);
                txtitemname.Text = mdr.GetString(2);
                cmbcatogory.Text = mdr.GetString(4);
                txtprice.Text = mdr.GetString(3);
               // itempk = mdr.GetString(0);

                if ((mdr != null))
                {
                    mdr.Close();
                }
              //  return;
            }
        
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
            
        }

        private void btnsave_Click(object sender, EventArgs e)
        {


            if (txtitemcode.Text == "")
            {
                MessageBox.Show("Please enter product Code ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtitemcode.Focus();
                return;
            }
            if (txtitemname.Text == "")
            {
                MessageBox.Show("Please enter Product Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtitemname.Focus();
                return;
            }
            if (txtprice.Text == "")
            {
                MessageBox.Show("Please enter price", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtprice.Focus();
                return;
            }
            try
            {
                if(txtitemcode.Text!=id){

                openCon();

                string ss = "select itemid from item where itemid='" + txtitemcode.Text + "'";

                mc = new MySqlCommand(ss, con);
                mdr = mc.ExecuteReader();
               
                if (mdr.Read())
                {
                    MessageBox.Show("Product Name Already Exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtitemcode.Text = "";
                    txtitemcode.Focus();


                    if ((mdr != null))
                    {
                        mdr.Close();
                    }
                    return;
                }
                closeCon();

               

                }
                if(s!=""){

                String q = "update item set itemid= '" + txtitemcode.Text + "',itemname='" + txtitemname.Text + "',price='" + Double.Parse(txtprice.Text) + "',catogory ='" + cmbcatogory.SelectedItem.ToString() + "' where itempk = '"+s+"'";
                    ExecuteQuery(q);
                    frmitemmanager.K = 1;

                }
                
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
                this.Close();
            }
           

        }
        //function to open connection
        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        //function to execute the insert update and delete query
        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Susscefully Updated");
                    this.Close();
                   
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }

        private void lblnew_Click(object sender, EventArgs e)
        {
            String s = Interaction.InputBox("Enter Your Catogary");
            String quary = "Insert into catogory (catogoryname) values ('" + s + "')";
            ExecuteQuery(quary);
            loadcatogory();
        }

        private void lblclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       




    }
}
