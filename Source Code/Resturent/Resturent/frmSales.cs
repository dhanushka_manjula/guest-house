﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading.Tasks;


using MySql.Data.MySqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



namespace Resturent
{
    public partial class frmsales : Form
    {
        MySqlConnection con;
        MySqlCommand mc;
       static   MySqlDataReader mdr;
        DataTable table;
        MySqlDataAdapter adapter;
       //.txtBillNo.Text = "0";


        public frmsales()
        {
            InitializeComponent();
            con = Connect.connect();
            
            dtpBillDate.Format = DateTimePickerFormat.Short;
            dtpBillDate.Value = DateTime.Today;

            loaditemname();
            getcusid();
            txtSubTotal.Text = "0";
            txtTotal.Text = "0";
            this.txtDiscountPer.Text = "0";
            this.txtDiscountAmount.Text = "0";
            this.txtTaxAmt.Text = "0";
            this.txtTaxPer.Text = "0";

        }

        public void loaditemname()
        {
            try
            {

                openCon();


                String s1 = "select * from item";

                mc = new MySqlCommand(s1, con);
                mdr = mc.ExecuteReader();
                cmbProductName.Items.Clear();
                cmbProductName.Items.Clear();
                while (mdr.Read())
                {


                    cmbProductName.Items.Add(mdr.GetString(2));
                    cmbitemid.Items.Add(mdr.GetString(1));

                }
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mdr.Close();
                closeCon();

            }
        }


        void getcusid()
        {

            try
            {

                openCon();






                txtBillNo.Text = DateTime.Now.ToString("yy/MM/dd-hh/mm/ss").Trim().Replace("/", "");
                   

                  

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mdr.Close();
                closeCon();
                cmbitemid.Select();
                
            }
            return;
        }


        private void Save_Click(object sender, EventArgs e)
        {
            //timer1.Stop;
            try
            {

                if (txtTaxPer.Text == "")
                {
                    MessageBox.Show("Please enter tax percentage", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTaxPer.Focus();
                    return;
                }
                if (txtDiscountPer.Text == "")
                {
                    MessageBox.Show("Please enter discount percentage", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDiscountPer.Focus();
                    return;
                }
                if (txtCash.Text == "")
                {
                    MessageBox.Show("Please enter Cash", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCash.Focus();
                    return;
                }

                if (ListView1.Items.Count == 0)
                {
                    MessageBox.Show("sorry no product added", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (txtSubTotal.Text != "")
                {
                    //dtpBillDate.Format = "";
                 //   DateTime time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); 

                    string cb = "insert Into bill(customid,date,SubTotal,Vatper,Vat,DiscountPer,Discount,Total,Cash,balance,Remark) VALUES ('" + txtBillNo.Text + "','" + DateTime.Now.ToString("yyyy/MM/dd") + "','" + txtSubTotal.Text + "','" + txtTaxPer.Text + "','" + txtTaxAmt.Text + "','" + txtDiscountPer.Text + "','" + txtDiscountAmount.Text + "','" + txtTotal.Text + "','" + txtCash.Text + "','" + txtChange.Text + "','" + txtRemarks.Text + "')";
                    ExecuteQuery(cb);


                    for (int i = 0; i <= ListView1.Items.Count - 1; i++)
                    {
                       
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        string cd = "insert Into ProductSold(BillID,ProductID,ProductName,Quantity,Price,TotalAmount) VALUES (@d1,@d2,@d3,@d4,@d5,@d6)";
                        mc = new MySqlCommand(cd, con);
                        mc.Connection = con;
                        mc.Parameters.AddWithValue("d1", txtBillNo.Text);
                        mc.Parameters.AddWithValue("d2", ListView1.Items[i].SubItems[1].Text);
                        mc.Parameters.AddWithValue("d3", ListView1.Items[i].SubItems[2].Text);
                        mc.Parameters.AddWithValue("d4", Int32.Parse(ListView1.Items[i].SubItems[4].Text));
                        mc.Parameters.AddWithValue("d5", Double.Parse(ListView1.Items[i].SubItems[3].Text));
                        mc.Parameters.AddWithValue("d6", Double.Parse(ListView1.Items[i].SubItems[5].Text));

                        mc.ExecuteNonQuery();
                        con.Close();
                    }

                    Reset();
                    getcusid();
                }
                //  Save.Enabled = false;
                //  btnPrint.Enabled = true;
             //   MessageBox.Show("Successfully Placed", "Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void txtSaleQty_TextChanged(object sender, EventArgs e)
        {

            try
            {
                double val1 = 0;
                double val2 = 0;
                double.TryParse(txtPrice.Text, out val1);
                double.TryParse(txtSaleQty.Text, out val2);
                val1 = Math.Round(val1, 2);
                val2 = Math.Round(val2, 2);
                double I = (val1 * val2);
                I = Math.Round(I, 2);
                txtTotalAmount.Text = I.ToString();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }


        private void Button7_Click(object sender, EventArgs e)
        {

            try
            {

                if (cmbProductName.Text == "")
                {
                    MessageBox.Show("Please select product name", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (txtSaleQty.Text == "")
                {
                    MessageBox.Show("Please enter quantity", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSaleQty.Focus();
                    return;
                }
                int SaleQty = Convert.ToInt32(txtSaleQty.Text);
                if (SaleQty == 0)
                {
                    MessageBox.Show("no. of quantity can not be zero", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSaleQty.Focus();
                    return;
                }


                String[] arr = new String[6];
                ListViewItem itm;
                arr[1] = cmbitemid.Text;
                arr[2] = cmbProductName.Text;
                arr[3] = txtPrice.Text;
                arr[4] = txtSaleQty.Text;
                arr[5] = txtTotalAmount.Text;
                itm = new ListViewItem(arr);
                ListView1.Items.Add(itm);



                txtTotal.Text = (Double.Parse(txtTotal.Text) + Double.Parse(txtTotalAmount.Text)).ToString();
                txtSubTotal.Text = txtTotal.Text;

                cmbitemid.Text = "";
                cmbProductName.Text = "";
                txtProductID.Text = "";
                txtPrice.Text = "";
                txtSaleQty.Text = "";
                txtTotalAmount.Text = "";

                btnRemove.Enabled = true;


                return;





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

            try
            {
                if (ListView1.Items.Count == 0)
                {
                    MessageBox.Show("No items to remove", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    String tot = this.ListView1.SelectedItems[0].SubItems[5].Text;
                    txtTotal.Text = (Double.Parse(txtTotal.Text) - Double.Parse(tot)).ToString();
                    txtSubTotal.Text = txtTotal.Text;
                    this.ListView1.SelectedItems[0].Remove();


                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtTaxPer_TextChanged(object sender, EventArgs e)
        {

            if (txtTaxPer.Text != "0" && txtTaxPer.Text != "")
            {
                try
                {

                    double vat = Double.Parse(txtTaxPer.Text);
                    double subtotal = Double.Parse(txtSubTotal.Text);

                    double totamon = (subtotal * vat) / 100;
                    txtTaxAmt.Text = totamon.ToString();

                    txtTotal.Text = (Double.Parse(txtTotal.Text) + totamon).ToString();


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = true;
        }





        private void Reset()
        {
           
            dtpBillDate.Text = DateTime.Today.ToString();

            cmbProductName.Text = "";
            txtProductID.Text = "";
            txtPrice.Text = "";

            txtSaleQty.Text = "";
            txtTotalAmount.Text = "";
            ListView1.Items.Clear();
            txtDiscountAmount.Text = "";
            txtDiscountPer.Text = "";

            txtSubTotal.Text = "0";
            txtTaxPer.Text = "0";
            txtTaxAmt.Text = "0";
            txtTotal.Text = "0";
            txtCash.Text = "";
            txtChange.Text = "";
            txtRemarks.Text = "";
            Save.Enabled = true;
            

          //  btnRemove.Enabled = false;
          //  btnPrint.Enabled = false;
            ListView1.Enabled = true;
            Button7.Enabled = true;

        }

        private void NewRecord_Click(object sender, EventArgs e)
        {
            this.ListView1.Clear();
            Reset();

        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {

            }
        }


        private void txtTotalPayment_TextChanged(object sender, EventArgs e)
        {

            try
            {

                if (txtCash.Text != "" && txtCash.Text != "0")
                {

                    double cash = Double.Parse(txtCash.Text);
                    double tot = Double.Parse(txtTotal.Text);
                    txtChange.Text = Math.Round((cash - tot),1).ToString();


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void btnPrint_Click(object sender, EventArgs e)
        {
            //timer1.Stop;
            try
            {

                if (txtTaxPer.Text == "")
                {
                    MessageBox.Show("Please enter tax percentage", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTaxPer.Focus();
                    return;
                }
                if (txtDiscountPer.Text == "")
                {
                    MessageBox.Show("Please enter discount percentage", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDiscountPer.Focus();
                    return;
                }
                if (txtCash.Text == "")
                {
                    MessageBox.Show("Please enter Cash", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCash.Focus();
                    return;
                }

                if (ListView1.Items.Count == 0)
                {
                    MessageBox.Show("sorry no product added", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (txtSubTotal.Text != "")
                {
                    //dtpBillDate.Format = "";
                    //   DateTime time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); 
                    string cb = "insert Into bill(customid,date,SubTotal,Vatper,Vat,DiscountPer,Discount,Total,Cash,balance,Remark) VALUES ('" + txtBillNo.Text + "','" + DateTime.Now.ToString("yyyy/MM/dd") + "','" + txtSubTotal.Text + "','" + txtTaxPer.Text + "','" + txtTaxAmt.Text + "','" + txtDiscountPer.Text + "','" + txtDiscountAmount.Text + "','" + txtTotal.Text + "','" + txtCash.Text + "','" + txtChange.Text + "','" + txtRemarks.Text + "')";
                    ExecuteQuery(cb);


                    for (int i = 0; i <= ListView1.Items.Count - 1; i++)
                    {

                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        string cd = "insert Into ProductSold(BillID,ProductID,ProductName,Quantity,Price,TotalAmount) VALUES (@d1,@d2,@d3,@d4,@d5,@d6)";
                        mc = new MySqlCommand(cd, con);
                        mc.Connection = con;
                        mc.Parameters.AddWithValue("d1", txtBillNo.Text);
                        mc.Parameters.AddWithValue("d2", ListView1.Items[i].SubItems[1].Text);
                        mc.Parameters.AddWithValue("d3", ListView1.Items[i].SubItems[2].Text);
                        mc.Parameters.AddWithValue("d4", Int32.Parse(ListView1.Items[i].SubItems[4].Text));
                        mc.Parameters.AddWithValue("d5", Double.Parse(ListView1.Items[i].SubItems[3].Text));
                        mc.Parameters.AddWithValue("d6", Double.Parse(ListView1.Items[i].SubItems[5].Text));


                        mc.ExecuteNonQuery();
                        
                    }

                   // Reset();
                  //  getcusid();

                   // Form1 f= new Form1()


                 ///   Cursor = Cursors.WaitCursor;
                   // frmbillprint frb = new frmbillprint(txtChange.Text);
                  //  frb.ShowDialog();
                 // Report.bill b = new Report.bill();
                //  b.SetParameterValue("id", txtBillNo.Text);
               //  b.PrintToPrinter(1, false, 0, 0);
                // Cursor = Cursors.Arrow;

                 ReportDocument cryRpt = new ReportDocument();
            cryRpt.Load("Report\\bill.rpt");



            cryRpt.SetDatabaseLogon("admin", "admin", "localhost", "resturent");

            ParameterFieldDefinitions crParameterFieldDefinitions;
            ParameterFieldDefinition crParameterFieldDefinition;
            ParameterValues crParameterValues = new ParameterValues();
            ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();



            crParameterDiscreteValue.Value = txtBillNo.Text;
            crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
            crParameterFieldDefinition = crParameterFieldDefinitions["id"];
            crParameterValues = crParameterFieldDefinition.CurrentValues;

            crParameterValues.Clear();
            crParameterValues.Add(crParameterDiscreteValue);
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);


          // Report.bill b = new Report.bill();
            // bill1.SetParameterValue("id", txtBillNo.Text);
         //  b.PrintToPrinter(1, false,0, 0);

         //  crystalReportViewer1.ReportSource = bill1;
         //  crystalReportViewer1.Refresh();

          // crystalReportViewer1.ReportSource = cryRpt;
          // crystalReportViewer1.Refresh();





         //   cryRpt.Load("Report\\bill.rpt");

                    /**
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;

            crConnectionInfo.ServerName = "Creative";
            crConnectionInfo.DatabaseName = "resturent";
            crConnectionInfo.UserID = "admin";
            crConnectionInfo.Password = "admin";

        

            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }
**/
          //  cryRpt.Refresh();
           // cryRpt.PrintToPrinter(2, true, 1, 2);
           cryRpt.PrintToPrinter(1, false, 0, 0);

           con.Close();


                  Reset();
                  getcusid();
                  //  f.crystalReportViewer1.ReportSource = repor1;
                  //  crystalReportViewer1.Refresh();

                //   bill.SetParameterValue("startdate", dtstart.Value);
                  //  repor1.SetParameterValue("enddate", dtstop.Value);
                  //  crystalReportViewer1.ReportSource = repor1;
                   // crystalReportViewer1.Refresh();


                }
                //  Save.Enabled = false;
                //  btnPrint.Enabled = true;
                //   MessageBox.Show("Successfully Placed", "Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }


        private void txtSaleQty_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }


        private void txtTaxPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }
        }

        private void txtDiscountPer_TextChanged(object sender, EventArgs e)
        {

            if (txtDiscountPer.Text != ""&&txtDiscountPer.Text != "0")
            {

                try
                {

                    double vat = Double.Parse(txtDiscountPer.Text);
                    double subtotal = Double.Parse(txtSubTotal.Text);

                    double totamon = (subtotal * vat) / 100;
                    txtDiscountAmount.Text = totamon.ToString();

                    txtTotal.Text = (Double.Parse(txtSubTotal.Text) - totamon + Double.Parse(txtTaxAmt.Text)).ToString();


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }


        }

        private void frmSales_Load(object sender, EventArgs e)
        {



        }


        private void cmbProductName_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
               // mdr.Close();
                if (mdr != null)
                {
                    mdr.Close();
                }

                openCon();

                String quary = "SELECT * from item WHERE itemname = '" + cmbProductName.Text + "'";
                mc = new MySqlCommand(quary, con);
               // mdr.Close();

               // mc.ExecuteReader();
                mdr = mc.ExecuteReader();

                if (mdr.Read())
                {
                    
                    txtPrice.Text = mdr.GetString(3);
                   cmbitemid.Text= mdr.GetString(1);


                    if (mdr != null)
                    {
                       mdr.Close();
                   }

                }
              // mdr1.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
              // mdr1.Close();
                closeCon();
            }



        }

        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allows 0-9, backspace, and decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {

        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

        }





        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }


        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                  //  MessageBox.Show("Query Executed");
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }

        private void cmbitemid_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (mdr != null)
                {
                    mdr.Close();
                }

                openCon();
               // mdr.Close();
                String quary = "SELECT * from item WHERE itemid = '" + cmbitemid.Text + "'";
                mc = new MySqlCommand(quary, con);

                mdr = mc.ExecuteReader();
               // mdr = 
              //  mdr.Close();

                if (mdr.Read())
                {
                  
                    cmbProductName.Text= mdr.GetString("itemname");
                  
                   if(mdr != null){
                        mdr.Close();
                   }
                   
                }

              //  mdr2.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
               // mdr2.Close();
                closeCon();
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Normal;
        }

        private void frmsales_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

            }


            if (this.WindowState == FormWindowState.Normal)
            {

                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }
        }

        private void cmbProductName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSubTotal_TextChanged(object sender, EventArgs e)
        {
            //timer1.Start();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {

        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {

        }



    }
}