﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Resturent
{
    public partial class frmitemmanager : Form
    {
        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;
        DataTable table;
        MySqlDataAdapter adapter;

       public  static int K=0;

        static string s;

        public frmitemmanager()
        {
            InitializeComponent();
            con = Connect.connect();




            tblitem.AutoGenerateColumns = false;
            tblitem.ColumnCount = 5;
            //   tblitem.ColumnWidth = 150;
            tblitem.Columns[0].HeaderText = "Product ID";
            tblitem.Columns[0].DataPropertyName = "itemid";
            tblitem.Columns[4].Name = "Price";


            tblitem.Columns[1].HeaderText = "Product Name";
            tblitem.Columns[1].DataPropertyName = "itemname";
            tblitem.Columns[2].HeaderText = "Catogary";
            tblitem.Columns[2].DataPropertyName = "catogory";
            tblitem.Columns[3].HeaderText = "Price";
            tblitem.Columns[3].DataPropertyName = "price";

            tblitem.Columns[4].HeaderText = "item key";
            tblitem.Columns[4].DataPropertyName = "itempk";
            tblitem.Columns[4].Visible = false;





            insertTable();
        }

       


       public void insertTable()
        {
            try
            {
                
                openCon();
                String query = "SELECT * from item";
                mc = new MySqlCommand(query, con);
                adapter = new MySqlDataAdapter(mc);
                table = new DataTable();
                adapter.Fill(table);


              //  DataTable dt = new DataTable();

                BindingSource bs = new BindingSource();

                bs.DataSource = table;

              //  dataGridView1.DataSource = bs;

               

                tblitem.DataSource = bs;
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }

        }



        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }



        private void lblfind_Click(object sender, EventArgs e)
        {

        }

        private void frmitemmanager_Load(object sender, EventArgs e)
        {

        }

        private void btnnew_Click(object sender, EventArgs e)
        {
            frmitem fm = new frmitem();

            //fm.Refresh();
            fm.ShowDialog();
        }

        private void btncatogory_Click(object sender, EventArgs e)
        {
            frmcatogary fm = new frmcatogary();
            fm.ShowDialog();
        }

  

        private void txtid_TextChanged(object sender, EventArgs e)
        {
            try
            {
                openCon();
                String s = txtid.Text;
                String query = "SELECT * from item where concat(itemid,itemname,catogory,price) like'%" + s + "%'";
                mc = new MySqlCommand(query, con);
                adapter = new MySqlDataAdapter(mc);
                table = new DataTable();
                adapter.Fill(table);


               

              //  tblitem.DataSource = table;


                tblitem.DataSource = table;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }


        }

        private void btndelete_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {


                try
                {
                   //DataGridViewCell oneCell = tblitem.SelectedCells[0];

                   // if(oneCell.Selected){
                    //txtname.Text = s;
                    //DataGridViewRow dv = this.tblitem.Rows[e.RowIndex];
                       
                    String q = "Delete from item where itempk = '" + s + "'";
                    ExecuteQuery(q);
                   //   this.tblitem.Rows.RemoveAt(oneCell.RowIndex);

                   // LoadPatientRecords();
                      //tblitem.Update();
                    //  tblitem.Refresh();
                      insertTable();
                  // }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    closeCon();
                }

                
            }

        }




        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Ok");
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        
        }

        private void tblitem_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
         //  s = tblitem.SelectedRows[0].Cells[0].Value.ToString();
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            frmitemupdate fm = new frmitemupdate();
            fm.ShowDialog();
        }

       public static string itemid()
        {
            return s;
        }

       private void tblitem_CellClick(object sender, DataGridViewCellEventArgs e)
       {
           try
           {

               if (e.RowIndex >= 0)
               {

               DataGridViewRow dv  = this.tblitem.Rows[e.RowIndex];
                   s = dv.Cells["Price"].Value.ToString();
                 //  txtname.Text = s;
               }
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }

       private void btnclose_Click(object sender, EventArgs e)
       {
           this.Close();
       }

       private void tblitem_CellContentClick(object sender, DataGridViewCellEventArgs e)
       {

       }

       public static void sta()
       {
         

       }

       private void time_Tick(object sender, EventArgs e)
       {
           if (K==1){
           insertTable();
           }
       }

       private void button1_Click(object sender, EventArgs e)
       {
           this.Close();

       }

       private void frmitemmanager_SizeChanged(object sender, EventArgs e)
       {

           if (this.WindowState == FormWindowState.Minimized)
           {
               this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

           }


           if (this.WindowState == FormWindowState.Normal)
           {

               this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
           }


       }

       private void groupBox1_Enter(object sender, EventArgs e)
       {
          
       }

       private void button2_Click(object sender, EventArgs e)
       {
           this.WindowState = FormWindowState.Minimized;
       }



      
    }
}
