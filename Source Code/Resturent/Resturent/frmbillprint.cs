﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Resturent
{
    public partial class frmbillprint : Form
    {
        String s;
        int r = 0;
        public frmbillprint(String s)
        {
            InitializeComponent();
            this.s = s;
        }

        private void frmbillprint_Load(object sender, EventArgs e)
        {
            this.label1.Text="Balance is Rs "+s;
            timer1.Start();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            r++;
            if(r==2){
                timer1.Stop();
                this.Close();
            }
        }
    }
}
