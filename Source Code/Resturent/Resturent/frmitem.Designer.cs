﻿namespace Resturent
{
    partial class frmitem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblitemname = new System.Windows.Forms.Label();
            this.lblcatogory = new System.Windows.Forms.Label();
            this.lblitemcode = new System.Windows.Forms.Label();
            this.lblprice = new System.Windows.Forms.Label();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtitemcode = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.lblnew = new System.Windows.Forms.Button();
            this.cmbcatogory = new System.Windows.Forms.ComboBox();
            this.lblclose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cachedReport1 = new CrystalDecisions.ReportSource.CachedReport();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblitemname
            // 
            this.lblitemname.AutoSize = true;
            this.lblitemname.Location = new System.Drawing.Point(14, 81);
            this.lblitemname.Name = "lblitemname";
            this.lblitemname.Size = new System.Drawing.Size(83, 18);
            this.lblitemname.TabIndex = 0;
            this.lblitemname.Text = "ITEM Name";
            // 
            // lblcatogory
            // 
            this.lblcatogory.AutoSize = true;
            this.lblcatogory.Location = new System.Drawing.Point(33, 121);
            this.lblcatogory.Name = "lblcatogory";
            this.lblcatogory.Size = new System.Drawing.Size(64, 18);
            this.lblcatogory.TabIndex = 1;
            this.lblcatogory.Text = "Catogory";
            // 
            // lblitemcode
            // 
            this.lblitemcode.AutoSize = true;
            this.lblitemcode.Location = new System.Drawing.Point(19, 45);
            this.lblitemcode.Name = "lblitemcode";
            this.lblitemcode.Size = new System.Drawing.Size(78, 18);
            this.lblitemcode.TabIndex = 2;
            this.lblitemcode.Text = "ITEM Code";
            // 
            // lblprice
            // 
            this.lblprice.AutoSize = true;
            this.lblprice.Location = new System.Drawing.Point(59, 156);
            this.lblprice.Name = "lblprice";
            this.lblprice.Size = new System.Drawing.Size(38, 18);
            this.lblprice.TabIndex = 3;
            this.lblprice.Text = "Price";
            // 
            // txtitemname
            // 
            this.txtitemname.Location = new System.Drawing.Point(100, 82);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(268, 25);
            this.txtitemname.TabIndex = 4;
            // 
            // txtprice
            // 
            this.txtprice.Location = new System.Drawing.Point(100, 155);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(147, 25);
            this.txtprice.TabIndex = 5;
            // 
            // txtitemcode
            // 
            this.txtitemcode.Location = new System.Drawing.Point(100, 47);
            this.txtitemcode.Name = "txtitemcode";
            this.txtitemcode.Size = new System.Drawing.Size(267, 25);
            this.txtitemcode.TabIndex = 6;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnsave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnsave.Location = new System.Drawing.Point(114, 197);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(84, 35);
            this.btnsave.TabIndex = 7;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // lblnew
            // 
            this.lblnew.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblnew.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblnew.Location = new System.Drawing.Point(383, 116);
            this.lblnew.Name = "lblnew";
            this.lblnew.Size = new System.Drawing.Size(75, 31);
            this.lblnew.TabIndex = 8;
            this.lblnew.Text = "New";
            this.lblnew.UseVisualStyleBackColor = false;
            this.lblnew.Click += new System.EventHandler(this.lblnew_Click);
            // 
            // cmbcatogory
            // 
            this.cmbcatogory.FormattingEnabled = true;
            this.cmbcatogory.Location = new System.Drawing.Point(100, 118);
            this.cmbcatogory.Name = "cmbcatogory";
            this.cmbcatogory.Size = new System.Drawing.Size(265, 26);
            this.cmbcatogory.TabIndex = 9;
            // 
            // lblclose
            // 
            this.lblclose.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblclose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblclose.Location = new System.Drawing.Point(216, 197);
            this.lblclose.Name = "lblclose";
            this.lblclose.Size = new System.Drawing.Size(75, 35);
            this.lblclose.TabIndex = 10;
            this.lblclose.Text = "Close";
            this.lblclose.UseVisualStyleBackColor = false;
            this.lblclose.Click += new System.EventHandler(this.lblclose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtitemcode);
            this.groupBox1.Controls.Add(this.cmbcatogory);
            this.groupBox1.Controls.Add(this.txtitemname);
            this.groupBox1.Controls.Add(this.lblclose);
            this.groupBox1.Controls.Add(this.lblnew);
            this.groupBox1.Controls.Add(this.lblitemcode);
            this.groupBox1.Controls.Add(this.lblcatogory);
            this.groupBox1.Controls.Add(this.lblitemname);
            this.groupBox1.Controls.Add(this.btnsave);
            this.groupBox1.Controls.Add(this.lblprice);
            this.groupBox1.Controls.Add(this.txtprice);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(12, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 300);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ITEM";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(489, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 25);
            this.button1.TabIndex = 12;
            this.button1.Text = "×";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmitem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(523, 332);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmitem";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ITEM";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblitemname;
        private System.Windows.Forms.Label lblcatogory;
        private System.Windows.Forms.Label lblitemcode;
        private System.Windows.Forms.Label lblprice;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtitemcode;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button lblnew;
        private System.Windows.Forms.ComboBox cmbcatogory;
        private System.Windows.Forms.Button lblclose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private CrystalDecisions.ReportSource.CachedReport cachedReport1;
    }
}