﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Resturent
{
    public partial class frmmain : Form
    {

        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;

        public frmmain()
        {
            InitializeComponent();
           // this.clo
            con = Connect.connect();
        }


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle |= 0x200 ;
                return myCp;
            }
        }

        private void itemManageToolStripMenuItem_Click(object sender, EventArgs e)
        {



            bool IsOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "Item Manager")
                {
                    IsOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (IsOpen == false)
            {
                frmitemmanager fm = new frmitemmanager();
                fm.MdiParent = this;
                fm.Show();
            }




         
        }

        private void catogoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmcatogary fm = new frmcatogary();
           // fm.MdiParent = this;
            fm.ShowDialog();

        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmchangeuser fm = new frmchangeuser();
          //  fm.MdiParent = this;
            fm.ShowDialog();
        }

        private void reportViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "Report")
                {
                    IsOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (IsOpen == false)
            {
                frmreport fm = new frmreport();
                fm.MdiParent = this;
                fm.Show();
            }

          
        }

        private void btnsales_Click(object sender, EventArgs e)
        {



            bool IsOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text =="Sales")
                {
                    IsOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (IsOpen == false)
            {
                frmsales f2 = new frmsales();
                f2.MdiParent = this;
                f2.Show();
            }


           

        }

        private void btnitem_Click(object sender, EventArgs e)
        {
            frmitemmanager fm = new frmitemmanager();
            fm.MdiParent = this;
            fm.Show();
        }

        private void btnreport_Click(object sender, EventArgs e)
        {
            frmreport fm = new frmreport();
            fm.MdiParent = this;
            fm.Show();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            frlogin fm = new frlogin();
            fm.MdiParent = this;
            fm.Show();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void itemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to Exit", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {

                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
            }
        }

        private void clearDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to clear this records?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {


                try
                {

                    String s = "delete from bill";
                    ExecuteQuery(s);
                    String s1 = "delete from productsold";
                    ExecuteQuery(s1);


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    closeCon();
                }

            }


        }






        //function to open connection
        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        //function to execute the insert update and delete query
        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Database Clear");
                    // this.txtitemcode.Text = "";
                    //  this.txtitemname.Text = "";
                    // this.txtprice.Text = "";
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
               // closeCon();
            }
        }

        private void calcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Calc.exe");
        }

        private void notePadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Notepad.exe");
        }

        private void wordPadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("WordPad.exe");
        }

        private void wordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("WinWord.exe");
        }

        private void frmmain_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Red;
        }





    }
}
