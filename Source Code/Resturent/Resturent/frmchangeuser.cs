﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Resturent
{
    public partial class frmchangeuser : Form
    {

        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;

        public frmchangeuser()
        {
            InitializeComponent();
            this.txtnewpassword.PasswordChar = '*';
            this.txtoldpassword.PasswordChar = '*';
            this.txtverifiednewpassword.PasswordChar = '*';
            con = Connect.connect();

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to change password?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                if (txtnewpassword.Text == "")
                {
                    MessageBox.Show("Please enter New Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtnewpassword.Focus();
                    return;
                }

                if (txtoldpassword.Text == "")
                {
                    MessageBox.Show("Please enter Old Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtoldpassword.Focus();
                    return;
                }

                if (txtverifiednewpassword.Text == "")
                {
                    MessageBox.Show("Please enter Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtverifiednewpassword.Focus();
                    return;
                }

                if (txtverifiednewpassword.Text != txtnewpassword.Text)
                {
                    MessageBox.Show("password not Match", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtverifiednewpassword.Focus();
                    return;
                }

                try
                {

                    openCon();

                    string ss = "select * from login where name ='admin'";

                    mc = new MySqlCommand(ss, con);
                    mdr = mc.ExecuteReader();

                    if (mdr.Read())
                    {

                        if (txtoldpassword.Text != mdr.GetString("password"))
                        {

                            MessageBox.Show("Wrong Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtoldpassword.Text = "";
                            txtoldpassword.Focus();
                        }



                        if ((mdr != null))
                        {
                            mdr.Close();
                        }
                       // return;
                    }
                    closeCon();


                    String q = "update login set password='" + txtnewpassword.Text + "' where name = 'admin'";
                    ExecuteQuery(q);


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    closeCon();
                   // this.Close();
                }
            }


        }




        //function to open connection
        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        //function to execute the insert update and delete query
        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Password updated");
                   // this.txtitemcode.Text = "";
                  //  this.txtitemname.Text = "";
                   // this.txtprice.Text = "";
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }




    }
}
