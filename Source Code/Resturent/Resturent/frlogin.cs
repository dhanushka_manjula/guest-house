﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Resturent
{

    
    public partial class frlogin : Form
    {
        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;

        public frlogin()
        {
            InitializeComponent();
            txtpassword.PasswordChar = '*';
            con = Connect.connect();
           // scon = con.connect();
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {

             if (txtusername.Text == "")
            {
                MessageBox.Show("Please enter User Name ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtusername.Focus();
                return;
            }
            if (txtpassword.Text == "")
            {
                MessageBox.Show("Please enter Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtpassword.Focus();
                return;
            }

            try
            {
                openCon();


                String s1 = "select * from login";

                mc = new MySqlCommand(s1, con);
                mdr = mc.ExecuteReader();
                String s="";
                if (mdr.Read())
                {


                   s= mdr.GetString("password");


                }
                closeCon();
               
                if (s.CompareTo(txtpassword.Text) == 0)
                {
                    frmmain fm = new frmmain();
                    fm.Show();

                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Wrong password");

                }
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
          
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
           // this.

            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }

        }



        //function to open connection
        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        //function to execute the insert update and delete query
        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                  //  MessageBox.Show("New Item Save");
                  
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }





    }
}
