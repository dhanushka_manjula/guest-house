﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


namespace Resturent
{
    public partial class frmreport : Form
    {
        public frmreport()
        {
            InitializeComponent();
            dtstart.Format = DateTimePickerFormat.Short;
            dtstart.Value = DateTime.Today;
            dtstop.Format = DateTimePickerFormat.Short;
            dtstop.Value = DateTime.Today;
        }

        private void btnview_Click(object sender, EventArgs e)
        {
           // repor1.SetParameterValue("startdate",dtstart.Value);
          //  repor1.SetParameterValue("enddate", dtstop.Value);

            ReportDocument cryRpt = new ReportDocument();
            cryRpt.Load("Report\\repor.rpt");

         //   TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
           // TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
           // ConnectionInfo crConnectionInfo = new ConnectionInfo();
          //  Tables CrTables;

            //   cryRpt.Load("PUT CRYSTAL REPORT PATH HERE\CrystalReport1.rpt");

          //  crConnectionInfo.ServerName = "Creative\\MySQL55";
          //  crConnectionInfo.DatabaseName = "resturent";
          //  crConnectionInfo.UserID = "admin";
          //  crConnectionInfo.Password = "admin";

          //  CrTables = cryRpt.Database.Tables;
           // foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
           // {
             //   crtableLogoninfo = CrTable.LogOnInfo;
             //   crtableLogoninfo.ConnectionInfo = crConnectionInfo;
              //  CrTable.ApplyLogOnInfo(crtableLogoninfo);
           // }
            cryRpt.SetDatabaseLogon("admin", "admin", "localhost", "resturent");

            ParameterFieldDefinitions crParameterFieldDefinitions;
            ParameterFieldDefinition crParameterFieldDefinition;
            ParameterValues crParameterValues = new ParameterValues();
            ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();



            crParameterDiscreteValue.Value = dtstart.Value;
            crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
            crParameterFieldDefinition = crParameterFieldDefinitions["startdate"];
            crParameterValues = crParameterFieldDefinition.CurrentValues;

            crParameterValues.Clear();
            crParameterValues.Add(crParameterDiscreteValue);
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);




            crParameterDiscreteValue.Value = dtstop.Value;
            crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
            crParameterFieldDefinition = crParameterFieldDefinitions["enddate"];
            crParameterValues = crParameterFieldDefinition.CurrentValues;



            crParameterValues.Clear();
            crParameterValues.Add(crParameterDiscreteValue);
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);







           






            crystalReportViewer1.ReportSource = cryRpt;
            crystalReportViewer1.Refresh();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void frmreport_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void frmreport_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

            }


            if (this.WindowState == FormWindowState.Normal)
            {

                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }

        }

        private void repor2_InitReport(object sender, EventArgs e)
        {

        }
    }
}
