﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Resturent
{
    public partial class frmcatogary : Form
    {

        MySqlConnection con;
        MySqlCommand mc;
        MySqlDataReader mdr;
        DataTable table;
        MySqlDataAdapter adapter;

        public frmcatogary()
        {
            InitializeComponent();
            con = Connect.connect();
            loaddata();
        }

        public void loaddata()
        {

            liview.Clear();
            liview.View = View.Details;
            liview.GridLines = true;
            liview.FullRowSelect = true;

            //Add column header
            liview.Columns.Add("No", 30);
            liview.Columns.Add("Catogory",120);


            //Add items in the listview
            String[] arr = new String[3];
            ListViewItem itm;

         

            try
            {

                openCon();


                String s1 = "select * from catogory";

                mc = new MySqlCommand(s1, con);
                mdr = mc.ExecuteReader();
                int k = 1;
            while (mdr.Read())
                {
                    //txtitemcode.Text = mdr.GetString(1);
                    arr[0] = k.ToString();
                        arr[1] = mdr.GetString(1);
                    itm = new ListViewItem(arr);
                    liview.Items.Add(itm);
                    k++;
                   // if ((mdr != null))
                   // {
                      //  mdr.Close();
                  //  }
                   // return;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmcatogary_Load(object sender, EventArgs e)
        {

        }

        private void btnnew_Click(object sender, EventArgs e)
        {
            String s = Interaction.InputBox("Enter Your Catogory");
            String quary = "Insert into catogory (catogoryname) values ('" + s + "')";
            ExecuteQuery(quary);
            loaddata();

        }


        public void openCon()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //function to Close connection
        public void closeCon()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        public void ExecuteQuery(string q)
        {
            try
            {
                openCon();
                mc = new MySqlCommand(q, con);
                if (mc.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Susscesfully");
                }
                else
                {
                    MessageBox.Show("Try Again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeCon();
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                String s = liview.SelectedItems[0].SubItems[1].Text;
                String quary = "delete from catogory where catogoryname ='" + s + "'";
                ExecuteQuery(quary);
                String quary1 = "update item set catogory ='' where catogory ='"+s+"'";
                ExecuteQuery(quary1);
                loaddata();
            }
            catch (ArgumentOutOfRangeException )
            {
                MessageBox.Show("Please Select a Catogory");
            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                String s = liview.SelectedItems[0].SubItems[1].Text;
                String s1 = Interaction.InputBox("Enter New Catogory");
                String quary = "update catogory set catogoryname='"+s1+"'  where catogoryname ='" + s + "'";
                ExecuteQuery(quary);
                String quary1 = "update item set catogory ='"+s1+"' where catogory ='" + s + "'";
                ExecuteQuery(quary1);
                loaddata();
            }
            catch (ArgumentOutOfRangeException )
            {
                MessageBox.Show("Please Select a Catogory");
            }
        }

    }
}
